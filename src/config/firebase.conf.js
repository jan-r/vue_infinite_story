import firebase from 'firebase/app'
import 'firebase/firestore'

let firebaseConfig = {
  apiKey: "AIzaSyBolx3gIe16v3tedWg785NIPpvnrzuh2GU",
  authDomain: "infinite-story.firebaseapp.com",
  databaseURL: "https://infinite-story.firebaseio.com",
  projectId: "infinite-story",
  storageBucket: "infinite-story.appspot.com",
  messagingSenderId: "723074554772"
};

let firestoreConfig = {
  timestampsInSnapshots: true
};

// nice docu fot the database
// https://firebase.google.com/docs/firestore/quickstart
firebase.initializeApp(firebaseConfig);

const firestore = firebase.firestore();
firestore.settings(firestoreConfig);

export { firebase, firestore }
