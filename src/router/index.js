import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase'

import Dashboard from '@/components/Dashboard'
import Story from '@/components/Story'
import Login from '@/components/Login'
import SignUp from '@/components/SignUp'
import Authorroom from '@/components/Authorroom'
import Writerroom from '@/components/Writerroom'

import StoryFooter from 'partials/StoryFooter'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '*',
      redirect: '/dashboard'
    }, {
      path: '/',
      redirect: '/dashboard'
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      components: {
        default: Dashboard,
      },
    },
    {
      path: '/story/:storyid',
      name: 'Story',
      components: {
        default: Story,
        footer: StoryFooter
      },
      props: { default: true, footer: true }
    },
    // {
    //   path: '/story/:storyid/continue',
    //   name: 'Authorroom',
    //   components: {
    //     default: Authorroom,
    //   },
    //   props: { default: true }
    // },
    {
      path: '/story/:storyid/continue/:resumptionid',
      name: 'Writerroom',
      components: {
        default: Writerroom,
      },
      props: { default: true }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/sign-up',
      name: 'SignUp',
      component: SignUp
    }
  ]
})

// router.beforeEach((to, from, next) => {
//   let currentUser = firebase.auth().currentUser;
//   let requiresAuth = to.matched.some(record => record.meta.requiresAuth);

//   if (requiresAuth && !currentUser) next('login')
//   else if (!requiresAuth && currentUser) next('dashboard')
//   else next()
// });

export default router;
