const state = {
  all: {},
  allResumptionIds: []
}


const mutations = {
  ADD_RESUMPTION(state, { resumption }) {
    if(!resumption) return false
    if(!state.allResumptionIds.includes(resumption.id)){
      state.all = { ...state.all, [resumption.id]: resumption.data() }
      state.allResumptionIds.push(resumption.id)
    }
  },
  REMOVE_RESUMPTION(state, { resumptionid }) {
    if (!resumption) return false
    if (state.allResumptionIds.includes(resumption.id)) {
      state.set('all', state.get('all').filter(o => o.get('id') !== resumptionid))
      state.allResumptionIds.remove(resumptionid)
    }
  },
  UPDATE_RESUMPTION(state, { resumptionid, resumptionFields }) {
    if (!resumptionid) return false
    console.log("update resumption", resumptionFields);

    state.all[resumptionid] = {
      ...state.all[resumptionid],
      ...resumptionFields
    }
  },
  CLEAR( state ) {
    state.all = { }
    state.allResumptionIds = []
  }
}

const actions = {
  async get({ commit, rootState }, storyid ) {
    if (!storyid) return false;
    let resumptionRef = rootState.db.collection('stories').doc(storyid).collection('resumptions')
    let resumptions = await resumptionRef.get()
    resumptions.forEach(resumption => commit('ADD_RESUMPTION', { resumption }))
  },
  // async getSingle({ commit, rootState }, {storyid, resumptionid} ) {
  //   if (!storyid || !paragraphid) return false;
  //   let paragraphRef = rootState.db.collection('stories').doc(storyid).collection('paragraphs').doc(paragraphid)
  //   let paragraph = await paragraphRef.get()
  //   commit('ADD_PARAGRAPH', { paragraph })
  // },
  async createResumption({ commit, rootState }, { author, created, text, storyId, locked, finished, branch }) {
    let resumptionObj = {
      author: author,
      created: created,
      text: text,
      storyid: storyid,
      locked: locked,
      finished, finished,
      branch: branch
    }
    const resumptionRef = await rootState.db.collection('stories').doc(storyId).collection('resumptions').add(resumptionObj)
    .then(res => {
      console.log('Resumption created.')
      commit('ADD_RESUMPTION', resumptionObj )
    })
    .catch(err => console.log('Error', err))
  },
  async deleteResumption({ commit, rootState }, resumptionid) {
    const resumptionRef = await rootState.db.collection('stories').doc(storyId).collection('resumptions').doc(resumptionid).delete()
      .then(res => {
        console.log('Resumption deleted.')
        commit('REMOVE_RESUMPTION', resumptionid)
      })
      .catch(err => console.log('Error', err))
  },
  async update({ commit, rootState }, { storyid, resumptionid, resumptionFields }) {
    if (!resumptionid || !storyid) return false
    let resumptionRef = rootState.db.collection('stories').doc(storyid).collection('resumptions').doc(resumptionid)
    resumptionRef.update(resumptionFields)
    commit('UPDATE_RESUMPTION', { resumptionid, resumptionFields })
  },
  addResumption( {commit}, resumption ){
    commit('ADD_RESUMPTION', { resumption } )
  },
  clear( { commit }){
    commit('CLEAR' )
  }

}
export default { namespaced: true, state, mutations, actions }
