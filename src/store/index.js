import Vue from 'vue'
import Vuex from 'vuex'
import { firebase, firestore } from '@/config/firebase.conf'

import users from './users'
import conversations from './conversations'
import stories from './stories'
import profile from './profile'
import paragraphs from './paragraphs'
import resumptions from './resumptions'

Vue.use(Vuex)
const state = {
  db: firestore
}

export default new Vuex.Store({
  state,
  modules: {
    users,
    conversations,
    stories,
    paragraphs,
    profile,
    resumptions
  }
})


//https://medium.com/js-dojo/build-a-realtime-chat-app-with-vuejs-vuex-and-firestore-32d081668709
