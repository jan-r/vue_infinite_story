const state = {
  all: {},
  current: {}
}

const mutations = {
  SET_USER(state, { user }) {
    state.all = { ...state.all, [user.id]: user.data() }
  },
  SET_CURRENT_USER(state, { user }) {
    state.current = { ...state.current, "current": user.data() }
  },
  CLEAR_CURRENT(state) {
    state.current = { }
  }
}


const actions = {
  // seed({ rootState }) {
	// },
  async get({ commit, rootState }) {
    let userRef = rootState.db.collection('users')
    let users = await userRef.get()
    users.forEach(user => commit('SET_USER', { user }))
  },
  async getSingle({ commit, rootState }, userid) {
    if(!userid) return false;
    let userRef = rootState.db.collection('users').doc(userid)
    let user = await userRef.get()
    commit('SET_USER', { user })
  },
  async getCurrent({ commit, rootState }, userid) {
    if(!userid) return false;
    let userRef = rootState.db.collection('users').doc(userid)
    let user = await userRef.get()
    commit('SET_CURRENT_USER', { user })
  }
}
export default {
  namespaced: true, state, mutations, actions
}
