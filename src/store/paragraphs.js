const state = {
  all: {},
  allParagraphIds: {}
}


const mutations = {
  ADD_PARAGRAPH(state, { storyid, paragraph }) {
    if(!paragraph) return false

    //neu
    if (!state.allParagraphIds[storyid] ){
      state.allParagraphIds[storyid] = []
    }
    //update
    if (!state.allParagraphIds[storyid].includes(paragraph.id)){
      // console.log(paragraph.data());
      state.all[storyid] = {
        ...state.all[storyid],
          [paragraph.id]: paragraph.data()
      }

      state.allParagraphIds[storyid].push(paragraph.id)
    }
  },
  CLEAR( state ) {
    state.all = { }
    state.allParagraphIds = []
  }
}

const actions = {
  // seed({ rootState }) {
  // },
  async get({ commit, rootState }, storyid ) {
    if (!storyid) return false;
    let paragraphRef = rootState.db.collection('stories').doc(storyid).collection('paragraphs')
    let paragraphs = await paragraphRef.get()
    paragraphs.forEach(paragraph => commit('ADD_PARAGRAPH', { storyid , paragraph }))
  },
  // async getSingle({ commit, rootState }, {storyid, paragraphid} ) {
  //   if (!storyid || !paragraphid) return false;
  //   let paragraphRef = rootState.db.collection('stories').doc(storyid).collection('paragraphs').doc(paragraphid)
  //   let paragraph = await paragraphRef.get()
  //   commit('ADD_PARAGRAPH', { paragraph })
  // },
  async createParagraph({ commit, rootState }, { author, created, text, storyid, branch, branchList }) {
    let paragraph = {
      author: author,
      created: created,
      text: text,
      branch: branch,
      branchList: branchList
    }

    const paragraphRef = await rootState.db.collection('stories').doc(storyid).collection('paragraphs').add(paragraph)
    .then(paragraphDoc => {
      console.log('Paragraph created.', paragraphDoc)
      commit('ADD_PARAGRAPH', { storyid, paragraphDoc } )
      rootState.db.collection('stories').doc(storyid).update({updated: Date.now()})
    })
    .catch(err => console.log('Error', err))
  },
  addParagraph( {commit}, { storyid, paragraph } ){
    // console.log("adding", {storyid, paragraph} );
    commit('ADD_PARAGRAPH', { storyid, paragraph } )
  },
  clear( { commit }){
    commit('CLEAR' )
  }

}
export default { namespaced: true, state, mutations, actions }
