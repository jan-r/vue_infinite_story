const state = {
  current: {},
}

const mutations = {
  SET_USER_ID(state, user) {
    state.current = { ...state.current, user }
  }
}


const actions = {
  // seed({ rootState }) {
	// },
}
export default {
  namespaced: true, state, mutations, actions
}
