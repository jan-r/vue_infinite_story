import Vue from 'vue'
import uuidv4 from 'uuid/v4'
const state = {
  all: {},
  allStoryIds: [],
  branchQueue: {}
}


const mutations = {
  ADD_STORY(state, { story }) {
    if(!story || !story.id) return false
    if (!state.allStoryIds.includes(story.id)){
      state.all = { ...state.all, [story.id]: story.data() }
      state.allStoryIds.push(story.id)
    }
  },
  UPDATE_STORY(state, { storyid, storyFields }){
    if (!storyid) return false
    state.all[storyid] = {
      ...state.all[storyid],
      ...storyFields
    }
  },
  ADD_BRANCH_QUEUE(state, { storyid, branch }){
    if(!storyid || !branch) return false
    //neu
    if (!state.branchQueue[storyid])
      state.branchQueue[storyid] = []
    //update
    state.branchQueue[storyid].push(branch)
  },
  CLEAR_BRANCH_STACK(state){
    state.branchQueue = {}
  }
}

const actions = {
  // seed({ rootState }) {
	// },
  async get({ commit, rootState }) {
    let storyRef = rootState.db.collection('stories')
    let stories = await storyRef.get()
    stories.forEach(story => commit('ADD_STORY', { story }))
  },
  async getSingle({ commit, rootState }, storyid ) {
    if(!storyid) return false;
    let storyRef = rootState.db.collection('stories').doc(storyid)
    let story = await storyRef.get()
    commit('ADD_STORY', { story })
  },
  async updateConv({ commit, rootState }, { storyid, convoid }){
    if(!storyid) return false
    let storyRef = rootState.db.collection('stories').doc(storyid)
    storyRef.update({ conversation: convoid })
    commit('UPDATE_STORY', {
      storyid: storyid,
      storyFields: { conversation: convoid }
    })
  },
  async createStory({ commit, rootState }, story ) {
    if (!story) return false

    let storyRef = rootState.db.collection('stories').add(story)
    commit('ADD_STORY', { storyRef })
  },
  addStory({commit, rootState}, story){
    if(!story) return false
    commit('ADD_STORY', { story } )
  },
  async update({commit, rootState}, { storyid, storyFields } ){
    if(!storyid) return false
    let storyRef = rootState.db.collection('stories').doc(storyid)
    storyRef.update( storyFields )
    commit('UPDATE_STORY', { storyid, storyFields })
  },
  addBranchQueue({commit}, {storyid, branch}){
    if(!storyid || !branch) return false
    console.log("adding new branch to local BranchQueue", { storyid, branch });
    commit('ADD_BRANCH_QUEUE', { storyid, branch })
  }

}
export default { namespaced: true, state, mutations, actions }
