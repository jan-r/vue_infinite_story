// import Vue from 'vue'
// import uuidv4 from 'uuid/v4'
const state = {
	all: {},
  currentMsgIds: [],
  currentMessages: []
}


const mutations = {
	SET_CONVERSATION(state, { conversation }) {
    const data = conversation.data()

    if(!data) return false;

		state.all = {
			...state.all,
			[conversation.id]: { users: data.users, created: data.created, messages: [] }
		}
	},
	ADD_MESSAGE(state, { conversationId, message }) {
    if(!conversationId || !message) return false;
    if (!state.currentMsgIds.includes(message.id)) {

      state.currentMessages = [message.data(), ...state.currentMessages]
      state.currentMsgIds.push(message.id)
		}
  },
  CLEAR_MESSAGES(state){
    state.currentMessages = []
    state.currentMsgIds = []

  }
}

const actions = {
	// seed({ rootState }) {
	// },
	async get({ commit, rootState }) {
    let convoRef = rootState.db.collection('conversations')
    let convos = await convoRef.get()
		convos.forEach(conversation => commit('SET_CONVERSATION', { conversation }))
  },
  //get explicit conversation
  async getSingle({ commit, rootState }, convoid) {
    if(!convoid) return false;
    let convoRef = rootState.db.collection('conversations').doc(convoid)
    let conversation = await convoRef.get()
    commit('SET_CONVERSATION', { conversation })
  },
  async getMessages({ commit, rootState }, convoid) {
    if (!convoid) return false;
    let messageRef = rootState.db.collection('conversations').doc(convoid).collection('messages')
    let messages = await messageRef.get()
    messages.forEach(message => commit('ADD_MESSAGE', { convoid,  message }))
  },
	sendMessage({ commit, rootState }, { text, created, sender, conversationId }) {
    const messageRef = rootState.db.collection('conversations').doc(conversationId).collection('messages').add({
      created: created,
      sender: sender,
      text: text,
    })

  },
  async create({ commit, rootState }, { created }){
    const convoRef = await rootState.db.collection('conversations').add({
      created: created
    })
    commit('CLEAR_MESSAGES')
    return convoRef
  }
}
export default { namespaced: true, state, mutations, actions }
