export default {
  data() {
    return {
      hasError: false,
      errorMessage: ""
    }
  },
  methods: {
    error: function(msg) {
      this.hasError = true;
      this.errorMessage = msg;
    },
    resolveError: function() {
      this.hasError = false;
      this.errorMessage = "";
    }
  }
};
