export default {
  methods: {
    lastUpdated(timestamp) {
      let now = new Date();
      let date = new Date(timestamp);
      let delta = (now - date) / 1000;

      let days = Math.floor(delta / 86400);
      let hours = Math.floor(delta / 3600) % 24;
      let minutes = Math.floor(delta / 60) % 60;

      if (days > 1) return days + ' Tagen';
      if (days > 0) return days + ' Tag';
      if (hours > 1) return hours + ' Stunden';
      if (hours > 0) return hours + ' Stunde';
      if (minutes > 1) return minutes + ' Minuten';
      if (minutes > 0) return minutes + ' Minute';
      return "wenigen Sekunden";
    },
    dateString: (timestamp) => {
      let str = new Date( timestamp*1 ).toLocaleDateString('de-DE');
      return str
    }
  }
};
