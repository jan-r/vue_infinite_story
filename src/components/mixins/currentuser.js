import { mapState } from 'vuex'
export default {
  data(){
    return{
      current_user_id: null
    }
  },
  created(){
    let user = this.$firebase.auth().currentUser;
    this.current_user_id = user ? user.uid : null;
    if (this.current_user_id){
      this.$store.dispatch('users/getCurrent', this.current_user_id)
    }else{
      this.$store.commit('users/CLEAR_CURRENT')
    }
  },
  computed: {
    ...mapState({
      //function, because we want to use "this" context
      user: state =>  state.users.current,
    }),
    notAllowed(){
      if(!this.current_user_id) return true
      else return false
    },
  },
  methods: {
    logout() {
      this.$firebase.auth().signOut();
    }
  },
};
