// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import { firebase, firestore } from '@/config/firebase.conf'
import store from '@/store'
import VueSweetalert2 from 'vue-sweetalert2';


firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    console.log("USER LOGIN");
    store.commit("profile/SET_USER_ID", { id: user.uid } )
  } else {
    console.log("NO USER LOGIN");
    store.commit("profile/SET_USER_ID", {} );
  }

  setup();

});


function setup(){
  Vue.config.productionTip = false;
  Vue.prototype.$firebase = firebase;

  Vue.use(VueSweetalert2);

  new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
  })
}


