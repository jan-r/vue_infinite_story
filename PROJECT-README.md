Besprechung Infinite Story

Probleme:
- Überblick geht verloren bei zu vielen Nachrichten 
L: Zusammenfassungen pflegen, Wordmap für Story, Genre durch Stichwörter


- neue Branches aus bestehenden Stories ermöglichen?
 (weniger übersichtlich)
L: dublizieren, falls gewünscht -> neuer Beitrag mit Zusammenfassung von Parent-Geschichte


- parallele Fortsetzung an gleichem Abschnitt
L: bei zeitgleicher Fortsetzung - Beitritt in "Author-Room", gleichzeitige Textbearbeitung | evtl mit Chat | blockieren ,Evtl Socket für direkten Austausch unter den Autoren (Nachrichten, neuer Text)



- Texteschränkung ?
L: max 150 Wörter, ~ 150*8 Zeichen


- Spam ?
L: Captcha


- Nachrichten Filter ?
L: Stoppwortreduktion, blockiere gesamte Nachricht, Möglichkeit zur Nachbearbeitung, evtl live


- Nachbearbeitung von Texten ?
L: eher nicht


Sonstiges
L: Score für Nutzer, Profit mit höherem Score, Glaubwürdigkeit bei Reporting und Posting erhöht

L: Kapitelmechanismus - neuer Einstiegspunkte, Zusammenfassungspunkt, Eingeleitet durch Nutzer, wenn


Technische Sicht
Story
	Beiträge( Text, Timestamp, User )

User( Nickname, E-Mail, Password )


Herausforderungen
- Author-Room für gleichzeitiges Bearbeiten
- livefeedback für Texte + Validierung
- Datenmodell
- Spamfilter

technische Mittel
Node 		-> weniger Reloads, Interaktivität
NoSql 		-> Datentypen können sich verändern, DB Schema in Dateien anpassbar, keinen externen DB Dienst hosten
git			-> bitbucket, Versionierung und Zusammenarbeit
sass		-> übersichtlichere, einfachere Handhabung von Stylings
Vue			-> Modularisierung, Übersichtlichkeit, CleanCode
Webpack		-> Entwicklungsunterstützung und Deployment
babel/ES6	-> funktionale Erweiterung von Javascript

Vorteile
Node läuft überall
Einsparung von Host-Werkzeuge wie Apache Server , MySQL Server